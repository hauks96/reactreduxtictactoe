import {MATCH_ENDED, NEW_MATCH} from "../constants";

/*
 * Triggered by 'new_match' socket listener.
 * Both the game reducer and the match reducer receive this action.
 * The game reducer uses it to remove the participants from the received invites list of the current session user.
 * The match reducer adds the new match to the match list.
 */
export const newMatch = (match) => {
    return {
        type: NEW_MATCH,
        payload: match
    }
}

/*
 * Triggered by 'match_ended' socket listener.
 * Both the game reducer and the match reducer receive this action.
 * The game reducer updates the game's state if it matches the matchID.
 * The match reducer updates the game with the given id
 */
export const matchEnded = (matchId, winner) => {
    return {
        type: MATCH_ENDED,
        payload: {
            matchId: matchId,
            winner: winner
        }
    }
}