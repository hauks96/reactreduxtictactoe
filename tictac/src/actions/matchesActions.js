import { LOAD_MATCHES } from "../constants";

export const loadMatches = (matches) => {
    return {
        type: LOAD_MATCHES,
        payload: matches,
    }
}
