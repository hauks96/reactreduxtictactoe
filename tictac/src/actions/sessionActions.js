import {
    CONNECT,
    CONNECTION_SUCCESS,
    CONNECTION_FAILURE,
    DISCONNECT,
    SESSION_CONNECT
} from "../constants";


// The socket middleware will intercept this before it goes into the reducer and try to connect the user
export const connectToSocket = ( username ) => {
    return {
        type: CONNECT,
        payload: username
    }
}

// The socket middleware will intercept this before it goes into the reducer and try to connect the user
export const connectSessionToSocket = () => {
    return {
        type: SESSION_CONNECT
    }
}

// The socket middleware will intercept this before it goes into the reducer and log out the user
export const disconnectFromSocket = () => {
    return {
        type: DISCONNECT
    }
}

// This action is triggered through a socket listener (see listeners/index.js)
export const connectionSuccess = ( userObject, existingUser ) => {
    return {
        type: CONNECTION_SUCCESS,
        payload: {
            username: userObject.username,
            userID : userObject.userID,
            existingUser: existingUser,
        }
    }
}

// This action is triggered through a socket listener (see listeners/index.js)
export const connectionError = () => {
    return {
        type: CONNECTION_FAILURE,
        payload: "Username is taken."
    }
}