import {
    ACCEPT_CHALLENGE, ASSIGN_SYMBOL, CLEAR_MATCH,
    DECLINE_CHALLENGE, MAKE_MOVE,
    OTHER_ACCEPTED,
    OTHER_DECLINED, PLAYER_READY,
    RECEIVE_CHALLENGE, RECEIVE_MOVE,
    SEND_CHALLENGE
} from "../constants";
import { v4 as uuidv4 } from 'uuid';

/*
 * This action is triggered by a user through a component when
 * the user wishes to challenge another player.
 * This action is intercepted by the socket middleware and it emits 'game_challenge' to another user.
 * The reducer then adds the invitation to the sent invites list.
 */
export const challengeUser = (userID) => {
    return {
        type: SEND_CHALLENGE,
        payload: userID
    }
}

/*
 * This action is triggered by the 'game_challenge' socket listener when
 * the user receives an invite from another player.
 * The action goes straight to the reducer where the invite gets added to the received invites list
 */
export const receiveChallenge =(challengerObject) => {
    return {
        type: RECEIVE_CHALLENGE,
        payload: challengerObject.challenger
    }
}

/*
 * This action is triggered by a user through a component.
 * Before it goes to the reducer it is intercepted by the socket middleware and
 * it emits 'game_challenge_declined' to the server before sending the action forward to the reducer.
 * The reducer then removes the invitation from the received invitations list.
 */
export const declineChallenge = (fromUserID) => {
    return {
        type: DECLINE_CHALLENGE,
        payload: fromUserID
    }
}

/*
 * Triggered by the 'game_challenge_declined' listener when
 * the user that the session user sent an invite to declines the invite.
 * The reducer then removes the invitation from the session users sent invites list.
 */
export const otherDeclinedChallenge = (fromUserSessionObject) => {
    return {
        type: OTHER_DECLINED,
        payload: fromUserSessionObject
    }
}


/*
 * Triggered by the user through a component.
 * Before the action goes to the reducer the middleware intercepts the action and
 * emits 'game_challenge_accepted' to the user that originally sent the invite.
 * The reducer then removes the invitation from the received invitations list.
 */
export const acceptChallenge = (fromUserSessionObject) => {
    return {
        type: ACCEPT_CHALLENGE,
        payload: {
            matchId: uuidv4(),
            opponent: fromUserSessionObject
        }
    }
}


/*
 * Triggered by the 'game_challenge_accepted' socket listener
 * This happens when the user that the session user sent an invite to accepts the invite.
 * The reducer removes the sent invitation from the session users sent invites list and
 * updates the match values with the match ID, opponent name and sets the game state as ongoing.
 */
export const otherAcceptedChallenge = (matchId, fromUserSessionObject) => {
    return {
        type: OTHER_ACCEPTED,
        payload: {
            matchId: matchId,
            opponent: fromUserSessionObject
        }
    }
}

/*
 * Triggered by either user in a game through a component.
 * This action is intercepted by the socket middleware and does NOT continue into the reducer.
 */
export const makeMove = (matchId, symbol, move, win, draw) => {
    return {
        type: MAKE_MOVE,
        payload: {
            matchId: matchId,
            symbol: symbol,
            move: move,
            win: win,
            draw: draw
        }
    }
}

/*
 * Triggered by 'game_move' socket listener.
 * Received on both ends of the game and updates the game state.
 * Goes straight to the reducer.
 */

export const receiveMove = (symbol, move) => {
    return {
        type: RECEIVE_MOVE,
        payload: {
            symbol: symbol,
            move: move
        }
    }
}

/*
 * Gets intercepted by the socket middleware and there the socket emits 'ready'.
 */
export const setReady = (matchId) => {
    return {
        type: PLAYER_READY,
        payload: matchId
    }
}


/*
 * Triggered by a socket listener 'assign_symbol' once the server sends back a response from the 'ready' emit.
 * Assigns the game reducers symbol.
 */
export const assignSymbol = (symbol) => {
    return {
        type: ASSIGN_SYMBOL,
        payload: symbol
    }
}


export const clearMatch = () => {
    return {
        type: CLEAR_MATCH
    }
}