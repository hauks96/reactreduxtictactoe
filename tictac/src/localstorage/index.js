const SESSION_DATA_KEY = "TicTacSession";
const SESSION_DATA_DEFAULT = "[]";

const session_format = {
    sessionID: 'string',
    userID: 'string',
    username: 'string',
    disconnected: 'boolean',
}

// Just validates whether the session instance received is on the correct format
function validate_session_instance(session_instance){
    if (typeof session_instance !== typeof session_format) {
        console.log("session instance not of same type as session format");
        return false;
    }
    let format_keys = Object.keys(session_format);
    let session_keys = Object.keys(session_instance);
    if (session_keys.length !== format_keys.length) {
        console.log("session instance has invalid amount of keys");
        return false;
    }

    for (let i=0; i<format_keys.length; i++)
    {
        let current_key = session_keys[i];
        if (!format_keys.includes(current_key))
            return false;

        let desired_type = session_format[current_key];
        if (typeof session_instance[current_key] !== desired_type)
        {
            console.log("Invalid type of key "+current_key+". Should be "+desired_type+" and is "+ typeof session_instance[current_key]);
            return false;
        }
    }
    return true;
}

// loads the sessions from localstorage and validates them
function load_sessions(){
    let session_data = localStorage.getItem(SESSION_DATA_KEY);
    if (session_data === null || session_data === undefined || session_data === "" || session_data === SESSION_DATA_DEFAULT)
    {
        console.log("No sessions");
        return [];
    }

    try {
        session_data = JSON.parse(session_data);
    }
    catch (e) {
        console.log("Session data corrupt. Resetting session data to default.");
        localStorage.setItem(SESSION_DATA_KEY, SESSION_DATA_DEFAULT)
        return [];
    }

    if (!session_data instanceof Array)
    {
        console.log("Session data corrupt. Resetting session data to default.");
        localStorage.setItem(SESSION_DATA_KEY, SESSION_DATA_DEFAULT)
    }
    let valid_sessions = [];
    for (let i=0; i<session_data.length; i++)
    {
        if (validate_session_instance(session_data[i]))
            valid_sessions.push(session_data[i]);
        else
            console.log("Corrupt session removed");
    }
    return valid_sessions;
}

// saves a new session to the current sessions
function save_session(sessions, new_session){
    if (!validate_session_instance(new_session))
    {
        console.log("Invalid session in addSession");
        return null;
    }
    for (let i=0; i<sessions.length; i++)
    {
        if ( sessions[i].username === new_session.username)
        {
            console.log("Username already in session storage");
            return null;
        }
    }

    sessions.push(new_session);
    localStorage.setItem(SESSION_DATA_KEY,JSON.stringify(sessions));
    console.log("Session saved");
    return new_session;
}

// writes the sessions array to localstorage
function update_current_session(sessions){
    localStorage.setItem(SESSION_DATA_KEY,JSON.stringify(sessions));
    console.log("Sessions updated");
}

// fetches a session in the sessions array and updates it's position in the array (sets it as the most recent)
function fetch_and_update_if_exists(sessions, sessionObject){
    if (sessions.length === 0) return false;
    for (let i=0; i<sessions.length; i++)
    {
        if (sessions[i].username === sessionObject.username)
        {
            sessions[i] = sessionObject;
            let new_latest_session = sessions.splice(i, 1)[0];
            sessions.push(new_latest_session);
            update_current_session(sessions);
            return true;
        }
    }
    return false;
}

// Gets a session by username in the sessions array. Does not update anything.
function get_user_session(sessions, username){
    if (sessions.length === 0) return null;
    for (let i=0; i<sessions.length; i++)
    {
        if (sessions[i].username === username)
        {
            return sessions[i];
        }
    }
    return null;
}

// Update disconnected of current session to true
function update_disconnected_true(sessions, current_session){
    current_session.disconnected = true; // Changed by reference.
    update_current_session(sessions);
}

// Update disconnected of current session to false
function update_disconnected_false(sessions, current_session){
    current_session.disconnected = false;
    update_current_session(sessions);
}

// Gets the most recent session, if it was active last time the user was on
function get_latest_session(sessions){
    if (sessions.length === 0) return null;
    if (!sessions[sessions.length-1].disconnected)
        return sessions[sessions.length-1];
    return null;
}

function remove_session(sessions, current_session){
    for (let i=0; i<sessions.length; i++){
        if (sessions[i].userID === current_session.userID)
        {
            sessions.splice(i, 1);
            update_current_session(sessions);
            return;
        }
    }
    console.log("Session to remove not found!");
}


/*
 * Allows multiple users on one browser. This is basically a user login api.
 * Handles all session related storage i/o and states.
 *
 * I made this with the intent of never deleting a user instance.
 * Meaning that when a user logs out he still exists on the localstorage.
 * So when he comes back the client knows he's an existing user.
 *
 * After I read the assignment description a bit more carefully I realized that
 * you actually require us to remove the user if he logs out, so this implementation is
 * kinda useless now :/
 */
const sessionAPI = () => {

    let sessions = load_sessions();
    let current_session = null;
    return {
        hasActiveSession: () => {
            return current_session !== null;
        },
        sessionExists: (username) => {
            /*
             * Can be used to check whether a user already exists or not in the session storage.
             * May be useful to display a welcome back message.
             */
            for (let i=0; i<sessions.length; i++)
            {
                if (sessions[i].username === username)
                {
                    return true;
                }
            }
            return false;
        },
        addSession: (sessionObject) => {
            /*
             * Add a session to the current session state.
             * After successfully invoking this method hasActiveSession will return true.
             * If the session already exists, it will update the session store state.
             * If the session didn't exist it will create it automatically.
             * This method fails silently so be sure to check your console for errors.
             */
            if (validate_session_instance(sessionObject))
            {
                if (fetch_and_update_if_exists(sessions, sessionObject))
                {
                    current_session = sessionObject;
                    return true;
                }
                else
                {
                    save_session(sessions, sessionObject);
                    current_session = sessionObject;
                    return true;
                }
            }
            console.log("Invalid session to add", sessionObject);
            return false;

        },
        getSession: (username) => {
            /*
            * Get a session by username on the pre condition that the session was not previously disconnected by the user.
            * Does not update any session states. Returns null if no session found.
            */
            return get_user_session(sessions, username);
        },
        getLatestSession: () => {
            /*
             * Get the latest session on the pre condition that the last used session was not disconnected by the user.
             * Does not update any session states. Returns null if no session found.
             */
            return get_latest_session(sessions);
        },
        clearSession: () => {
            /*
             * Clear the current session if any session is active.
             */
            current_session = null;
        },
        removeSession: () => {
            /*
             * Removes the currently action session from the localstorage.
             */
            remove_session(sessions, current_session);
        },
        updateDisconnectedTrue: () => {
            /*
             * Update the current sessions disconnected state to true.
             * This means that once the user reconnects to the client again he will have to log in manually.
             */
            if (!current_session)
            {
                console.log("Must fetch or add user session with get/addSession before updating.");
                return;
            }
            update_disconnected_true(sessions, current_session);
        },
        updateDisconnectedFalse: () => {
            /*
             * Update the current sessions disconnected state to false.
             * This means that once the user reconnects to the client again he will be connected automatically.
             */
            if (!current_session)
            {
                console.log("Must fetch or add user session with get/addSession before updating.");
                return;
            }
            update_disconnected_false(sessions, current_session);
        }
    }
}

export default sessionAPI;