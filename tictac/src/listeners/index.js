import {connectionError, connectionSuccess } from "../actions/sessionActions";
import {addUser, disconnectedUser, loadUsers, removeUser} from "../actions/usersActions";
import { loadMatches } from "../actions/matchesActions";
import {
    assignSymbol,
    otherAcceptedChallenge,
    otherDeclinedChallenge,
    receiveChallenge,
    receiveMove
} from "../actions/gameActions";
import {matchEnded, newMatch} from "../actions/sharedActions";

/*
 * Add the action dispatchers to the socket's on.x methods.
 */
export const listen = (store, socket, session) =>
{
    socket.on('session', (userObject) => {
        let isExistingUser = session.sessionExists(userObject.username);
        session.addSession({...userObject, disconnected: false});
        store.dispatch(connectionSuccess(userObject, isExistingUser));
    });

    socket.on('connect_error', () => {
        store.dispatch(connectionError());
    });

    socket.on('disconnected_user', (userID) => {
        store.dispatch(disconnectedUser(userID));
    });

    socket.on('connected_user', userObject => {
        console.log("User reconnected");
        store.dispatch(addUser(userObject));
    });

    socket.on('users', userList => {
        store.dispatch(loadUsers(userList));
    });

    socket.on('matches', matches => {
        store.dispatch(loadMatches(matches));
    });

    socket.on('game_challenge', challenger => {
        store.dispatch(receiveChallenge(challenger));
    });

    socket.on('game_challenge_declined', fromUserObject => {
        store.dispatch(otherDeclinedChallenge(fromUserObject));
    });

    socket.on('game_challenge_accepted', (matchId, fromUserObject) => {
        store.dispatch(otherAcceptedChallenge(matchId, fromUserObject));
    });

    socket.on('match_ended', (matchId, winner) => {
        console.log("MATCH ENDED IN LISTENER!");
        store.dispatch(matchEnded(matchId, winner));
    });

    socket.on('new_match', match => {
        store.dispatch(newMatch(match));
    });

    socket.on('user_left', userID => {
        store.dispatch(removeUser(userID))
    })

    socket.on('game_move', (symbol, move) => {
       store.dispatch(receiveMove(symbol, move));
    });

    socket.on('assign_symbol', symbol => {
        store.dispatch(assignSymbol(symbol));
    })
}