import { Switch, Route } from 'react-router-dom';
import Home from "./views/Home";
import Container from "./components/Container";
import Dashboard from "./views/Dashboard";
import Match from "./views/Match";
import NotFound from "./components/NotFound";
import React from "react";
import Heading from "./components/Heading";
import { WithAuth, NoAuth } from "./authorization";
import NavigationBar from "./components/Navigation";

const App = () => {
    return (
        <>
            <NavigationBar/>
            <Container styleCLS="light-drop-shadow main-container" layer={1}>
                <Heading/>
                <Switch>
                    <Route exact path="/" component={ NoAuth(Home) }/>
                    <Route exact path="/dashboard" component={ WithAuth(Dashboard) }/>
                    <Route exact path="/match/:matchId" component={ WithAuth(Match) }/>
                    <Route path="*" component={ NotFound }/>
                </Switch>
            </Container>
        </>
    );
}

export default App;
