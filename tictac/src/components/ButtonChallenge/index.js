import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {acceptChallenge, declineChallenge} from "../../actions/gameActions";
import {faBan, faCheck} from "@fortawesome/free-solid-svg-icons";
import Button from "../Button";
import React from "react";
import styles from './ButtonChallenge.module.css';
import PropTypes from 'prop-types';
import {useDispatch} from "react-redux";

const ButtonChallenge = ({user, outerStyle}) => {
    let dispatch = useDispatch();
    let text = (
        <>
            <span className={styles.username}>
                {user.username}
            </span>
            <span className={styles.infoText}>
                has challenged you!
            </span>
        </>
    );

    let accept_decline = (
        <div className={styles.accDecContainer}>
            <div onClick={() => dispatch(acceptChallenge(user))}
                 className={styles.accept}>
                <FontAwesomeIcon icon={faCheck}/>
            </div>
            <div onClick={() => dispatch(declineChallenge(user.userID))}
                 className={styles.decline}>
                <FontAwesomeIcon icon={faBan}/>
            </div>
        </div>);

    return (
        <div className={outerStyle}>
            <Button width={"w100"}
                    noAnimation={true}
                    text={text}
                    icon={accept_decline}
                    onClickHandler={() => null}
                    color={"purple"}/>
        </div>

    );
};

ButtonChallenge.propTypes = {
    // The user object
    user: PropTypes.object.isRequired,
    // set margin and paddings
    outerStyle: PropTypes.string
}

export default ButtonChallenge;