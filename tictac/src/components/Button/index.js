import styles from './Buttons.module.css';
import PropTypes from 'prop-types';
import React from 'react';

const colors = ["red", "blue", "green", "purple", "orange", "grey"];

class Button extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hovering: false
        }
    }
    render(){
        let width = this.props.width ? styles[this.props.width] : "";
        return (
            <div className={styles.buttonWrapper + " " + width}>
                <button onMouseOver={() => this.setState({hovering: true})}
                        onMouseLeave={ () => this.setState({hovering:false})}
                        onClick={ () => this.props.disabled ? null : this.props.onClickHandler() }
                        className={styles[this.props.color] + " " +
                                   styles.button + " " +
                                   this.props.styleCLS + " " +
                                   width}
                        disabled={this.props.disabled? this.props.disabled : false}>

                    <div className={styles.buttonText}>
                        {this.props.text}
                        <div className={
                            this.state.hovering && !this.props.noAnimation && !this.props.disabled?
                                styles.mask + " " + styles.animateButton
                                : ""}>
                        </div>
                    </div>

                    { this.props.icon
                        ?
                        <div className={styles.buttonIcon}>
                            {this.props.icon}
                        </div>
                        :
                        <>
                        </>
                    }

                </button>
            </div>
            );
    }
}


Button.propTypes = {
    // Button text
    text: PropTypes.any.isRequired,
    // Button icon - font awesome icon
    icon: PropTypes.any,
    // onClickHandler
    onClickHandler: PropTypes.func.isRequired,
    // color of button
    color: PropTypes.oneOf(colors).isRequired,
    // style class
    styleCLS: PropTypes.string,
    // Disable animation by setting false
    noAnimation: PropTypes.bool,
    // Set custom width in percentages
    width: PropTypes.oneOf(["w25", "w50", "w75", "w100"]),
    // disabled
    disabled: PropTypes.bool
}

export default Button;