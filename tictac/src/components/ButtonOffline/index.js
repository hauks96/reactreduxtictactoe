import Button from "../Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUserSlash} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import PropTypes from 'prop-types';
import styles from './ButtonOffline.module.css';

const ButtonOffline = ({user, outerStyle}) => (
    <div className={outerStyle}>
        <Button width={"w100"}
                text={
                    <div className={styles.text}>
                        <div className={styles.username}>{user.username}</div>
                    </div>}
                disabled={true}
                icon={<FontAwesomeIcon icon={faUserSlash}/>}
                onClickHandler={ () => null }
                color={"grey"}/>
    </div>

);

ButtonOffline.propTypes = {
    // The user object
    user: PropTypes.object.isRequired,
    // set margin and paddings
    outerStyle: PropTypes.string
}

export default ButtonOffline;