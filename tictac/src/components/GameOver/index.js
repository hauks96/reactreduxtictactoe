import styles from './GameOver.module.css';
import React from 'react';
import { Modal } from 'infinity-modules-hauks96';
import Button from "../Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight, faMedal, faSadTear} from "@fortawesome/free-solid-svg-icons";
import {Link, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {clearMatch} from "../../actions/gameActions";

const victory_icon = <div className={styles.victoryMedal}><FontAwesomeIcon icon={faMedal}/></div>
const defeat_icon = <div className={styles.defeatSadFace}><FontAwesomeIcon icon={faSadTear}/></div>

class GameOver extends React.Component {
    constructor(props) {
        super(props);
        this.display_timeout = null;
        this.closeModal = this.closeModal.bind(this);
        this.state = {
            modal_open: false,
            redirect: false,
            game_over_title: "",
            game_over_message: ""
        }
    }

    closeModal(){
        this.props.clearMatch();
        this.setState({
            redirect: true,
            modal_open: false
        });
    }
    componentWillUnmount() {
        clearTimeout(this.display_timeout);
    }

    findAppropriateMessage(){
        // there was a winner
        if (this.props.game.winner){
            // this user lost
            if (this.props.game.winner.username !== this.props.session.username){
                this.setState({
                    modal_open: true,
                    game_over_title: <div className={styles.defeat + " title"}>{defeat_icon}DEFEAT!</div>,
                    game_over_message: this.props.game.winner.username + " slaughtered you in a game of tic tac toe!"
                });
                return;
            }
            // this user won
            this.setState({
                modal_open: true,
                game_over_title: <div className={styles.victory + " title"}>{victory_icon}VICTORY!</div>,
                game_over_message: "You rekt "+this.props.game.opponent.username + " in a match of tic tac toe. Well done!"
            });
            return;
        }
        // match was a draw
        this.setState({
            modal_open: true,
            game_over_title: <div className={styles.draw + " title"}>DRAW!</div>,
            game_over_message: "You both lost!... Or you both won?"
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.game.winner === "none" && this.props.game.winner !== "none"){
            if (this.props.game.opponentLeft){
                this.setState({
                    modal_open: true,
                    game_over_title:
                        <div className={styles.victory + " title"}>{victory_icon}VICTORY!</div>,
                    game_over_message: "Your opponent left the match... What a chicken!"
                });
                return;
            }

            this.display_timeout = setTimeout(() => {
                this.findAppropriateMessage();
            }, 1500);
        }
    }

    render() {
        if (this.state.redirect){
            return <Redirect to={"/dashboard"}/>
        }

        return (
            <Modal id="modalOpacity" isOpen={this.state.modal_open} onClose={ () => this.closeModal() }>
                <Modal.Title>
                    {this.state.game_over_title}
                </Modal.Title>
                <Modal.Body>
                    {this.state.game_over_message}
                </Modal.Body>
                <Modal.Footer>
                    <Link to={"/dashboard"} style={{textDecoration: 'none'}}>
                        <div className={styles.footerPadding}>
                            <Button text={"Back to dashboard"}
                                    onClickHandler={()=>this.closeModal()}
                                    color={"orange"}
                                    width={"w100"}
                                    icon={<FontAwesomeIcon icon={faArrowRight}/>}/>
                        </div>
                    </Link>
                </Modal.Footer>
            </Modal>
        );
    }

}

const MapStateToProps = (state) => {
    return {
        session: state.session,
        game: state.game
    }
}

const MapDispatchToProps = (dispatch) => {
    return {
        clearMatch: () => dispatch(clearMatch())
    }
}

export default connect(MapStateToProps, MapDispatchToProps)(GameOver);