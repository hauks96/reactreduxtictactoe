import styles from './Heading.module.css';

const Heading = () => (
    <div className={styles.heading + " title"}>
        Tic Tackity Toe
    </div>
);

export default Heading;