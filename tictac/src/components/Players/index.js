import styles from './Players.module.css';
import {useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircle} from "@fortawesome/free-regular-svg-icons";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import React from "react";

const o = <FontAwesomeIcon icon={faCircle}/>;
const x = <FontAwesomeIcon icon={faTimes}/>;

const Players = () => {
    const game = useSelector(({game}) => game);
    const session = useSelector(({session}) => session);
    let userSymbol = "Please wait...";
    let opponentSymbol = "Please wait...";
    if (game.symbol){
        userSymbol = game.symbol === "X" ? x : o;
        opponentSymbol = game.symbol === "X" ? o : x;
    }

    return (
        <div className={styles.pLayout + " title"}>
            <div className={styles.user}>
                <div>{session.username}</div>
                <div className={styles.userSymbol}>{userSymbol}</div>
            </div>
            <div className={styles.vs}>VS</div>
            <div className={styles.opponent}>
                <div>{game.opponent ? game.opponent.username: "?"}</div>
                <div className={styles.opponentSymbol}>{opponentSymbol}</div>
            </div>
        </div>
        );

};

export default Players;