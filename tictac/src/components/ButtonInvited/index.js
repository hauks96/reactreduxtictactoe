import Button from "../Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import PropTypes from 'prop-types';
import styles from './ButtonInvited.module.css';

const ButtonInvited = ({user, outerStyle}) => (
    <div className={outerStyle}>
        <Button width={"w100"}
                text={
                    <div className={styles.text}>
                        <div className={styles.username}>{user.username}</div>
                        <div className={styles.inviteSent}>Invite sent!</div>
                    </div>}
                disabled={true}
                icon={<FontAwesomeIcon icon={faCheckCircle}/>}
                onClickHandler={ () => null }
                color={"green"}/>
    </div>

);

ButtonInvited.propTypes = {
    // The user object
    user: PropTypes.object.isRequired,
    // set margin and paddings
    outerStyle: PropTypes.string
}

export default ButtonInvited;