import Container from "../Container";
import React from "react";
import styles from './UserList.module.css';
import {useSelector} from "react-redux";
import ButtonChallenge from "../ButtonChallenge";
import ButtonInvited from "../ButtonInvited";
import ButtonInvite from "../ButtonInvite";
import ButtonOffline from "../ButtonOffline";

const UserList = () => {
    const game = useSelector(({game}) => game);
    const users = useSelector(({users}) => users);
    const user_session = useSelector(({session}) => session);

    const wasInvitedBy = (userId) => {
        let invites = game.receivedInvites;
        for (let i=0; i<invites.length; i++)
        {
            if (invites[i].userID === userId)
                return true;
        }
        return false;
    }
    const hasInvited = (userID) => {
        let invites = game.sentInvites;
        for (let i=0; i<invites.length; i++)
        {
            if (invites[i] === userID)
                return true;
        }
        return false;
    }

    return (
        <Container styleCLS={styles.userList} layer={0}>
            <h3 className={styles.online}>Online users</h3>
            <div className={styles.users}>
                {users.map(user => {
                    if (user.username === user_session.username) return <div key={user.userID}>{""}</div>
                    if (!user.connected) return <ButtonOffline outerStyle={styles.buttonOuterStyle} key={user.userID} user={user}/>
                    if (hasInvited(user.userID)) return <ButtonInvited outerStyle={styles.buttonOuterStyle} key={user.userID} user={user}/>
                    if (wasInvitedBy(user.userID)) return <ButtonChallenge outerStyle={styles.buttonOuterStyle} key={user.userID} user={user}/>
                    return <ButtonInvite outerStyle={styles.buttonOuterStyle} key={user.userID} user={user}/>
                })}
            </div>
        </Container>
    )

}

export default UserList;
