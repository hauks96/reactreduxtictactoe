import React from 'react';
import "./navigation.module.css";
import { useSelector, useDispatch } from "react-redux";
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt, faGamepad } from '@fortawesome/free-solid-svg-icons';
import { disconnectFromSocket } from "../../actions/sessionActions";
import styles from './navigation.module.css';

const NavigationBar = () => {
    const session = useSelector(({session}) => session);
    const dispatch = useDispatch();

    return (
        <nav className={"navbar navbar-expand-lg primary-drop-shadow "+styles.navbar}>
            <Link to={ session.connection.connected ? "/dashboard" : "/" }>
                <div className={"navbar-brand brand "+ styles.brand}>
                    <span className={styles.brandIcon}><FontAwesomeIcon icon={faGamepad}/></span>
                    { session.connection.connected
                        ? <span className={"title "+styles.brandName}>{session.username}</span>
                        : <></> }
                </div >
            </Link>

            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">{""}</div>

            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon">{""}</span>
            </button>

            { session.connection.connected
                ?
                <div onClick={() => dispatch(disconnectFromSocket()) } className={styles.signOut}>
                    <span className={"title "+styles.signOutText}>Logout</span>
                    <FontAwesomeIcon icon={faSignOutAlt}/>
                </div>
                :
                <></>
            }
        </nav>
    );
}

export default NavigationBar;