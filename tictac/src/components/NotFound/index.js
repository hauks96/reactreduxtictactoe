import styles from './NotFound.module.css';

const NotFound = () => (
    <h3 className={styles.notFound}>Page does not exist.</h3>
);

export default NotFound;