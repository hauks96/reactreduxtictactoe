import React from 'react';
import {connect} from 'react-redux';
import {add_move} from "./tt_logic";
import styles from './TicTacBoard.module.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {faCircle} from "@fortawesome/free-regular-svg-icons";
import {makeMove, setReady} from "../../actions/gameActions";


class TicTacBoard extends React.Component {
    constructor(props) {
        super(props);
        this.board_columns = [];
        this.o = <FontAwesomeIcon icon={faCircle}/>
        this.x = <FontAwesomeIcon icon={faTimes}/>
        this.state = {
            hasMove: false,
            gameManager: null,
            gameOver: false,
        }
    }

    componentDidMount() {
        for (let i=0; i<9; i++)
            this.board_columns.push(i+1);

        this.setState({
            gameManager: {
                horizontal: [0,0,0], // [1,2,3] [4,5,6] [7,8,9]
                vertical: [0,0,0],   // [1,4,7] [2,5,8] [3,6,9]
                diagonalLR: 0,       // 1,5,9
                diagonalRL: 0,       // 3,5,7
                total: 0,
                isWin: false,
                isDraw: false,
            }
        });
        this.props.alert_ready(this.props.game.matchId);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.game.opponentMoves.length < this.props.game.opponentMoves.length){
            let new_move = this.props.game.opponentMoves[this.props.game.opponentMoves.length-1];
            const gameManager = add_move(new_move, {...this.state.gameManager}, this.props.game.opponent.symbol)

            if (gameManager.isWin || gameManager.isDraw){
                this.setState({
                    gameOver: true,
                    hasMove: true,
                    gameManager: gameManager
                });
            }
            else {
                this.setState({
                    hasMove: true,
                    gameManager: gameManager
                });
            }
        }

        if (prevProps.game.symbol===null && this.props.game.symbol!==null) {
            this.setState({
                hasMove: this.props.game.symbol === "X"
            });
        }
        // other left so we win
        if (prevProps.game.opponentLeft === false && this.props.game.opponentLeft === true){
            this.props.make_move(this.props.game.matchId, this.props.game.symbol,0 , true, false);
        }
    }

    makeAMove(move){
        if (!this.state.hasMove) return;
        if (this.state.gameManager.isDraw ||this.state.gameManager.isWin){
            return;
        }
        const gameManager = add_move(move, this.state.gameManager, this.props.game.symbol);

        if (gameManager.isWin || gameManager.isDraw){
            this.setState({
                gameOver: true,
                gameManager: gameManager,
                hasMove: false,
            });
        }
        else {
            this.setState({
                gameManager: gameManager,
                hasMove: false,
            });
        }

        let game = this.props.game;
        this.props.make_move(game.matchId, game.symbol, move, gameManager.isWin, gameManager.isDraw && !gameManager.isWin);
    }

    getSymbol(){
        return this.props.game.symbol === "O" ? this.o : this.x;
    }
    getOpponentSymbol(){
        return this.props.game.symbol === "O" ? this.x : this.o;
    }

    render() {
        return (
            <div className={styles.game}>
                { this.state.hasMove||this.state.gameOver ? <></> : <span className={styles.wait}>Opponent's turn.</span> }
                { this.state.gameOver ? <div className={styles.gameover + " title"}>GAME OVER</div> : <></>}
                <div className={styles.board}>
                    { this.board_columns.map( (col) => {
                        if (this.props.game.userMoves.includes(col))
                            return (<div key={col} className={styles.userMove}>{ this.getSymbol() }</div>);

                        if (this.props.game.opponentMoves.includes(col))
                            return (<div key={col} className={styles.opponentMove}>{ this.getOpponentSymbol() }</div>);

                        return (
                            <div key={col} onClick={() => this.makeAMove(col)}
                                 className={styles.empty}>
                                {""}
                            </div>);
                    })}
                </div>
            </div>
        )
    }
}

const MapStateToProps = (state) => {
    return {
        game: state.game,
    }
}

const MapDispatchToProps = (dispatch) => {
    return {
        make_move: (matchId, symbol,move, win, draw) => dispatch(makeMove(matchId, symbol, move, win, draw)),
        alert_ready: (matchId) => dispatch(setReady(matchId))
    }
}

export default connect(MapStateToProps, MapDispatchToProps)(TicTacBoard);