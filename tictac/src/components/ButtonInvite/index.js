import Button from "../Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPaperPlane} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import PropTypes from 'prop-types';
import {challengeUser} from "../../actions/gameActions";
import {useDispatch} from "react-redux";

const ButtonInvite = ({user, outerStyle}) => {
    const dispatch = useDispatch();
    return (
        <div className={outerStyle}>
            <Button width={"w100"}
                    text={user.username}
                    icon={<FontAwesomeIcon icon={faPaperPlane}/>}
                    onClickHandler={ () => dispatch(challengeUser(user.userID)) }
                    color={"blue"}/>
        </div>
    );

};

ButtonInvite.propTypes = {
    // The user object
    user: PropTypes.object.isRequired,
    // set margin and paddings
    outerStyle: PropTypes.string
}

export default ButtonInvite;