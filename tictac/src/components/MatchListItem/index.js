import PropTypes from 'prop-types';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMedal, faQuestion} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import styles from './MatchListItem.module.css';

const no_icon = <div className={styles.statusIcon}>{""}</div>;
const in_game_icon = <div className={styles.statusIcon}><FontAwesomeIcon icon={faQuestion}/></div>;
const winner_icon = <div className={styles.statusIcon}><FontAwesomeIcon icon={faMedal}/></div>;

const iconMap = {
    "-1":in_game_icon,
     "0":winner_icon,
     "1":winner_icon,
     "2":no_icon
}

const colorMap = {
    "-1": styles.active,
    "0": styles.neutral,
    "1": styles.neutral,
    "2": styles.neutral
}


// -1 in game, 0 player 1 won, 1 player 2 won, 2 draw
const MatchListItem = ({winner, matchParticipants, isOngoing}) => {
    let winnerIndex = "-1";
    if (!isOngoing){
        if (!winner) winnerIndex = "2";
        else winnerIndex = winner.userID === matchParticipants[0].userID ? "0" : "1";
    }
    let color = colorMap[winnerIndex];
    let icon = iconMap[winnerIndex];
    let p1_icon = winnerIndex === "1" ? no_icon : icon;
    let p2_icon = winnerIndex === "0" ? no_icon : icon;

    return (
        <div className={color}>
            <div className={styles.player1}>
                { p1_icon }
                {matchParticipants[0].username}
            </div>
            <div className={styles.vs + " title"}>
                VS
            </div>
            <div className={styles.player2}>
                {matchParticipants[1].username}
                { p2_icon }
            </div>
        </div>
    )

}

MatchListItem.propTypes = {
    // the winner object or undefined
    winner: PropTypes.any,
    // The two participants of the game
    matchParticipants: PropTypes.array.isRequired,
    // is match ongoing or not
    isOngoing: PropTypes.bool.isRequired
}

export default MatchListItem;