import Container from "../Container";
import React from "react";
import styles from './MatchList.module.css';
import MatchListItem from "../MatchListItem";
import {useSelector} from "react-redux";

const MatchList = () => {
    const matches = useSelector(({matches}) => matches);
    return (
        <Container styleCLS={styles.matchList} layer={0}>
            <h3 className={styles.contentTitle}>Matches</h3>
            <div className={styles.matches}>
                {matches.map(match => (
                    <MatchListItem key={match.matchId}
                                   isOngoing={match.isOngoing}
                                   matchParticipants={match.participants}
                                   winner={match.winner}/>)
                )}
            </div>
        </Container>
        );
};

export default MatchList;
