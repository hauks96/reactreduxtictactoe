// socket reducer
export const CONNECT            = "CONNECT";
export const SESSION_CONNECT    = "SESSION_CONNECT";
export const CONNECTION_SUCCESS = "CONNECTION_SUCCESS";
export const CONNECTION_FAILURE = "CONNECTION_FAILURE";
export const DISCONNECT         = "DISCONNECT";
export const DISCONNECTED_USER  = "DISCONNECTED_USER";

// users reducer
export const ADD_USER    = "ADD_USER";
export const REMOVE_USER = "REMOVE_USER";
export const LOAD_USERS  = "LOAD_USERS";

// Matches reducer
export const LOAD_MATCHES = "LOAD_MATCHES";

// Game reducer
export const SEND_CHALLENGE        = "SEND_CHALLENGE";
export const RECEIVE_CHALLENGE     = "RECEIVE_CHALLENGE";
export const ACCEPT_CHALLENGE      = "ACCEPT_CHALLENGE";
export const DECLINE_CHALLENGE     = "DECLINE_CHALLENGE";
export const OTHER_DECLINED        = "OTHER_DECLINED";
export const OTHER_ACCEPTED        = "OTHER_ACCEPTED";
export const CLEAR_MATCH           = "CLEAR_MATCH";
export const MAKE_MOVE             = "MAKE_MOVE";
export const RECEIVE_MOVE          = "RECEIVE_MOVE";
export const ASSIGN_SYMBOL         = "ASSIGN_SYMBOL";
export const PLAYER_READY          = "PLAYER_READY";

// Shared
export const NEW_MATCH = "NEW_MATCH";
export const MATCH_ENDED = "MATCH_ENDED";
