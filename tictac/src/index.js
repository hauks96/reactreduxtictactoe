import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from "react-router-dom";
import thunk from 'redux-thunk';
import SocketMiddleware from './socketMiddleware';
import reducers from './reducers';

const middleware = [thunk, SocketMiddleware];

ReactDOM.render(
  <React.StrictMode>
    <Provider store={ createStore( reducers, compose(applyMiddleware(...middleware)) )}>
        <Router>
            <App />
        </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
