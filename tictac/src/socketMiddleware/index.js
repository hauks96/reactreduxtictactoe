import { io } from 'socket.io-client';
import {
    SEND_CHALLENGE,
    CONNECT,
    DISCONNECT,
    SESSION_CONNECT,
    DECLINE_CHALLENGE,
    MAKE_MOVE,
    ACCEPT_CHALLENGE, PLAYER_READY
} from "../constants";
import { listen } from "../listeners";
import sessionAPI from '../localstorage';
const server_domain = "http://localhost:8080";

const SocketMiddleware = () => {
    let socket = null;
    let session = sessionAPI();

    /* Middleware intercepts all dispatches and modifies/does something for the ones it desires. */
    return store => next => (action) =>
    {
        switch (action.type) {
            case SESSION_CONNECT:
                let prev_session = session.getLatestSession();

                if (!prev_session)
                    break;

                if (socket)
                {
                    if (socket.connected) break;
                    else socket = null;
                }

                socket = io(server_domain, {
                    autoConnect: false,
                    auth: { sessionID: prev_session.sessionID, username: prev_session.username },
                });
                listen(store, socket, session);

                socket.connect();
                socket.emit('users');
                socket.emit('matches');
                return next(action);
            case CONNECT:
            {
                if (socket)
                {
                    if (socket.connected) break;
                    else socket = null;
                }

                let existing_session = session.getSession(action.payload);
                if (existing_session)
                {
                    socket = io(server_domain, {
                        autoConnect: false,
                        auth: { sessionID: existing_session.sessionID },
                    });
                }
                else
                {
                    socket = io(server_domain, {
                        autoConnect: false,
                        auth: { username: action.payload },
                    });
                }

                listen(store, socket, session);


                socket.connect();
                socket.emit('users');
                socket.emit('matches');
                return next(action);
            }
            case DISCONNECT:
            {
                //session.updateDisconnectedTrue(); - Uncomment for storing users after logout (multiuser support)
                socket.emit('leave');
                session.removeSession();         // Comment out for storing users after logout (multiuser support)
                session.clearSession();
                socket.disconnect();
                return next(action);
            }
            case SEND_CHALLENGE:
                socket.emit('game_challenge', action.payload);
                return next(action);
            case DECLINE_CHALLENGE:
                socket.emit('game_challenge_declined', action.payload);
                return next(action);
            case ACCEPT_CHALLENGE:
                socket.emit('game_challenge_accepted', action.payload.matchId, action.payload.opponent.userID);
                return next(action);
            case MAKE_MOVE:
                let match = action.payload;
                socket.emit('game_move', match.matchId, match.symbol, match.move, match.win, match.draw);
                break;
            case PLAYER_READY:
                socket.emit('ready', action.payload);
                break;
            default:
                // Don't do anything
                return next(action);
        }
    }
}

export default SocketMiddleware();