import {ADD_USER, REMOVE_USER, LOAD_USERS, DISCONNECTED_USER} from "../constants";

function contains(userList, user)
{
    for (let i=0; i<userList.length; i++)
        if (userList[i].username === user.username) return true;
    return false;
}

function updateUserStatus(userList, userID, isConnected)
{
    let newList = [...userList];
    for (let i=0; i<newList.length; i++)
    {
        if (newList[i].userID === userID) {
            newList[i].connected = isConnected;
            break;
        }
    }
    return newList;
}


export default function usersReducer(state=[], action)
{
    switch(action.type)
    {
        case LOAD_USERS:
            return action.payload;
        case DISCONNECTED_USER:
            return updateUserStatus(state, action.payload, false);
        case REMOVE_USER:
            return state.filter((user) => user.userID !== action.payload);
        case ADD_USER:
            if (contains(state, action.payload)) return updateUserStatus(state, action.payload.userID, true);
            return [...state, action.payload];
        default:
            return state;
    }
}