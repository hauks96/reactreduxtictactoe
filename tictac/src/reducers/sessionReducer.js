import {
    CONNECT,
    CONNECTION_FAILURE,
    CONNECTION_SUCCESS,
    DISCONNECT,
    SESSION_CONNECT
} from '../constants';

const connection_state = {
    connected: false,
    error: false,
    error_message: "",
    wasSessionAttempt: false,
    user_disconnect: false,
}

const default_match = {
    inGame: false,
    matchId: null,
}

const session_state = {
    userID: null,
    username: null,
    existingUser: false,
    connection: connection_state,
    match: default_match
}

export default function sessionReducer(state= session_state, action)
{
    switch(action.type)
    {
        case SESSION_CONNECT:
            return {...state, connection: {...state.connection, wasSessionAttempt: true }};
        case CONNECT:
            return {...state, username: action.payload, connection: connection_state };
        case DISCONNECT:
            return {...session_state, connection: {...connection_state, user_disconnect: true}};
        case CONNECTION_SUCCESS:
            return {
                ...state,
                userID: action.payload.userID,
                username: action.payload.username,
                existingUser: action.payload.existingUser,
                connection: {...connection_state, connected: true}
            };
        case CONNECTION_FAILURE:
            return {
                ...state,
                connected: false,
                connection: {...state.connection, error: true, connected: false, error_message: action.payload } };
        default:
            return state;
    }
}