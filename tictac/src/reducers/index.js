import { combineReducers } from 'redux';
import session from './sessionReducer';
import matches from './matchesReducer';
import users from './usersReducer';
import game from './gameReducer';

export default combineReducers({
    session,
    matches,
    users,
    game
});