import {
    ACCEPT_CHALLENGE, ASSIGN_SYMBOL, CLEAR_MATCH, DECLINE_CHALLENGE, DISCONNECT, DISCONNECTED_USER, MATCH_ENDED,
    NEW_MATCH,
    OTHER_ACCEPTED,
    OTHER_DECLINED,
    RECEIVE_CHALLENGE, RECEIVE_MOVE, REMOVE_USER,
    SEND_CHALLENGE,
} from "../constants";

const default_state = {
    winner: "none",
    isOngoing: false,
    matchId: null,
    symbol: null,
    opponent: null,
    opponentLeft: false,
    userMoves: [],
    opponentMoves: [],
    receivedInvites: [], // session object list
    sentInvites: [] // userID list
}

function remove_from_received(list, opponent_userID) {
    return list.filter( (inviter) => inviter.userID !== opponent_userID);
}

function remove_from_sent(list, opponent_userID) {
    return list.filter( (invitation) => invitation !== opponent_userID );
}

function add_sent_invite(list, received_invite) {
    for (let i=0; i<list.length; i++)
    {
        if (received_invite === list[i].userID)
            return list;
    }
    return [...list, received_invite];
}

function add_received_invite(list, received_invite)
{
    for (let i=0; i<list.length; i++)
    {
        if (received_invite.userID === list[i].userID)
            return list;
    }
    return [...list, received_invite];
}

export default function gameReducer(state=default_state, action)
{
    switch(action.type)
    {
        case SEND_CHALLENGE:
            return {...state, sentInvites: add_sent_invite(state.sentInvites, action.payload)};
        case RECEIVE_CHALLENGE:
            return {...state, receivedInvites: add_received_invite(state.receivedInvites, action.payload)};
        case DECLINE_CHALLENGE:
            return {...state, receivedInvites: remove_from_received(state.receivedInvites, action.payload)};
        case ACCEPT_CHALLENGE:
        case OTHER_ACCEPTED:
            return {
                ...state,
                matchId: action.payload.matchId,
                opponent: action.payload.opponent,
                isOngoing: true,
                sentInvites: [],
                receivedInvites: remove_from_received(state.receivedInvites, action.payload.opponent.userID)};
        case OTHER_DECLINED:
            return {...state, sentInvites: remove_from_sent(state.receivedInvites, action.payload.userID)};
        case NEW_MATCH:
            return {
                ...state,
                receivedInvites: state.receivedInvites.filter( (inviter) =>
                    inviter.userID!==action.payload.participants[0].userID &&
                    inviter.userID!==action.payload.participants[1].userID)}
        case MATCH_ENDED:
            if (action.payload.matchId === state.matchId)
                return {...state, winner: action.payload.winner, isOngoing: false};
            return state;
        case CLEAR_MATCH:
            return {...default_state, receivedInvites: state.receivedInvites, sentInvites: state.sentInvites };
        case RECEIVE_MOVE:
            if (action.payload.symbol === state.symbol)
                return {...state, userMoves: [...state.userMoves, action.payload.move]};
            return {...state, opponentMoves: [...state.opponentMoves, action.payload.move]};
        case ASSIGN_SYMBOL:
            let opponent_symbol = action.payload === "O" ? "X" : "O";
            return {...state, symbol: action.payload, opponent: {...state.opponent, symbol: opponent_symbol}};
        case DISCONNECT:
            return default_state;
        case DISCONNECTED_USER:
        case REMOVE_USER:
            let opponentLeft = false;
            if (state.opponent)
                opponentLeft = state.opponent.userID === action.payload;
            return {...state,
                opponentLeft: opponentLeft,
                sentInvites: remove_from_sent(state.sentInvites, action.payload),
                receivedInvites: remove_from_received(state.receivedInvites, action.payload)};
        default:
            return state;
    }
}
