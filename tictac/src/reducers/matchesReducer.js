import {LOAD_MATCHES, MATCH_ENDED, NEW_MATCH} from "../constants";


function find_and_update(matches, match)
{
    let new_arr = [];
    for (let i=0; i<matches.length; i++)
    {
        if (matches[i].matchId === match.matchId)
        {
            let updated_match = {...matches[i], winner: match.winner, isOngoing: false };
            new_arr.push(updated_match);
        }
        else new_arr.push(matches[i]);
    }
    return new_arr;
}

export default function matchesReducer(state=[], action)
{
    switch(action.type)
    {
        case LOAD_MATCHES:
            return action.payload;
        case NEW_MATCH:
            return [action.payload, ...state];
        case MATCH_ENDED:
            return find_and_update(state, action.payload);
        default:
            return state;
    }
}