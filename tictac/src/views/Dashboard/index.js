import React from 'react';
import UserList from "../../components/UserList";
import { useSelector } from "react-redux";
import styles from './Dashboard.module.css';
import {Redirect} from 'react-router-dom';
import MatchList from "../../components/MatchList";

const Dashboard = () => {
    const game = useSelector(({game}) =>game);

    if (game.isOngoing)
        return <Redirect to={`match/${game.matchId}`}/>

    return (
        <>
            <div className={styles.dashboard}>
                <UserList/>
                <MatchList/>
            </div>
        </>
    );
}

export default Dashboard;
