import React from 'react';
import { connect } from 'react-redux';
import Container from "../../components/Container";
import { connectSessionToSocket, connectToSocket} from "../../actions/sessionActions";
import styles from './Home.module.css';
import { faWifi } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Button from "../../components/Button";

class Home extends React.Component {
    state = {
        username_input: "",
        input_error: "",
    }

    componentDidMount() {
        this.props.connectSessionToSocket();
    }

    submitInput(){
        if (this.state.username_input === "")
            this.setState({input_error: "Username is required."});

        else if (this.state.username_input.length <= 3)
            this.setState({input_error: "Name must be at least 4 characters."});

        else
        {
            this.setState({ input_error: "" });
            this.props.connectToSocket(this.state.username_input);
        }
    }

    render(){
        let input_error = <></>;
        if (this.state.input_error !=="")
            input_error = <div className={styles.error}>{this.state.input_error}</div>

        let connection_error = <></>;
        if (this.props.session.connection.error)
            connection_error = <div className={styles.error}>{this.props.session.connection.error_message}</div>;

        return (
            <Container styleCLS={styles.homeScreen + " primary-drop-shadow"} layer={0}>
                <h3>Log in</h3>
                <label className={styles.inputLabel} htmlFor="username">Enter a username</label>
                <input id="username"
                       className={`form-control w-75`}
                       type={'text'}
                       onInput={ (e) => this.setState({ username_input: e.target.value })}
                       defaultValue={this.state.username_input}
                       placeholder="username..."/>
                {connection_error}
                {input_error}
                <div className={styles.buttonContainer}>
                    <Button text={"Connect"}
                            color={"green"}
                            onClickHandler={ () => this.submitInput() }
                            icon={<FontAwesomeIcon icon={faWifi}/>}/>
                </div>
            </Container>
        );
    }
}

const MapStateToProps = (state) => {
    return {
        session: state.session,
        users: state.users,
    }
}

const MapDispatchToProps = (dispatch) => {
    return {
        connectToSocket: (username) => dispatch(connectToSocket(username)),
        connectSessionToSocket: () => dispatch(connectSessionToSocket()),
    }
}

export default connect(MapStateToProps, MapDispatchToProps)(Home);