import styles from './Match.module.css';
import TicTacBoard from "../../components/TicTacBoard";
import Players from "../../components/Players";
import GameOver from "../../components/GameOver";

const Match = () => {
    return (
        <div className={styles.match}>
            <Players/>
            <TicTacBoard/>
            <GameOver/>
        </div>
    );
}

export default Match;