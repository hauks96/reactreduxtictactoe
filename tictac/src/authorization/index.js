import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import React from 'react';

export function WithAuth(WrappedComponent) {
    return connect(MapStateToProps, null)
        (class extends React.Component {
        render() {
            if (!this.props.session.connection.connected)
            {
                return (
                    <Redirect to="/"/>
                );
            }
            return (
                <WrappedComponent/>
            );
        }
    });
}

export function NoAuth(WrappedComponent) {
    return connect(MapStateToProps, null)
        (class extends React.Component {
        render() {
            if (this.props.session.connection.connected)
            {
                return (
                    <Redirect to="/dashboard"/>
                );
            }
            return (
                <WrappedComponent/>
            );
        }
    });
}

const MapStateToProps = (state) => {
    return {
        session: state.session
    }
}